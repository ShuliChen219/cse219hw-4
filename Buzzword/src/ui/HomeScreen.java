package ui;

/**
 * @author  Shuli Chen
 */

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

public class HomeScreen extends Application {

    final static private int BUTTON_WIDTH = 40;
    final static private int BUTTON_HEIGHT = 40;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("BuzzWord Game");
        primaryStage.setWidth(800);
        primaryStage.setHeight(600);

        Label createNewLabel = new Label("Create New Profile");
        createNewLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        createNewLabel.setPrefWidth(190);
        createNewLabel.setPrefHeight(40);

        Label loginLabel = new Label("Login");
        loginLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        loginLabel.setPrefWidth(190);
        loginLabel.setPrefHeight(40);
        loginLabel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                LoginFrame loginFrame = new LoginFrame();
                loginFrame.start(new Stage());
            }
        });

        Label professaurLabel = new Label("Professaur");
        professaurLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        professaurLabel.setPrefWidth(190);
        professaurLabel.setPrefHeight(40);

        final ComboBox modeSelect = new ComboBox();
        modeSelect.getItems().addAll(
                "English Dictionary",
                "Piaces",
                "Science",
                "Famous People"
        );
        modeSelect.setValue("Select Mode");
        modeSelect.setPrefHeight(40);
        modeSelect.setPrefWidth(190);
        modeSelect.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        modeSelect.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
                    @Override
                    public ListCell<String> call(ListView<String> param) {
                        final ListCell<String> cell = new ListCell<String>() {
                            {
                                super.setPrefWidth(190);
                            }
                            @Override
                            public void updateItem(String item,
                                                   boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item);
                                    setTextFill(Color.DARKRED);
                                    setStyle("-fx-background-color:DimGray");
                                }
                                else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });

        Label startLabel = new Label("Start Playing");
        startLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        startLabel.setPrefWidth(190);
        startLabel.setPrefHeight(40);
        startLabel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                GamePlayScreen gamePlayScreen = new GamePlayScreen();
                try {
                    gamePlayScreen.start(new Stage());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        VBox leftMenu = new VBox(createNewLabel, loginLabel, professaurLabel, modeSelect, startLabel);
        leftMenu.setPrefWidth(220);
        leftMenu.setPrefHeight(300);
        leftMenu.setMaxSize(220, 300);
        leftMenu.setSpacing(20);
        leftMenu.setAlignment(Pos.TOP_RIGHT);
        leftMenu.setStyle("-fx-background-color:DarkGray");

        HBox leftPanel = new HBox(leftMenu);
        leftPanel.setPrefWidth(220);
        leftPanel.setAlignment(Pos.CENTER);
        leftPanel.setStyle("-fx-background-color:DarkGray");

        GridPane keyboardPanel = new GridPane();
        keyboardPanel.setPrefWidth(580);
        keyboardPanel.setHgap(40);
        keyboardPanel.setVgap(40);
        keyboardPanel.setAlignment(Pos.CENTER);
        //keyboardPanel.setStyle("-fx-background-color:gray");

        Label title = new Label("!! Buzz Word !!");
        title.setStyle("-fx-text-fill:white");
        title.setAlignment(Pos.CENTER);
        title.setPrefWidth(580);
        title.setPrefHeight(50);

        FlowPane titlePanel = new FlowPane(title);
        titlePanel.setPrefHeight(100);
        titlePanel.setPrefWidth(580);
        titlePanel.setStyle("-fx-background-color:gray");

        VBox rightPanel = new VBox(titlePanel, keyboardPanel);
        rightPanel.setPrefWidth(580);
        rightPanel.setStyle("-fx-background-color:gray");

        /*
        for(int i = 1; i <= 9; ++i){
            Button btn = new Button(String.valueOf(i));
            btn.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);
            btn.setOnAction(e -> label.setText(label.getText() + btn.getText()));
            keyboardPanel.add(btn, (i - 1) % 3, i > 3 ? (i > 6 ? 2 : 1) : 0);
        }
        */

        Button button1 = new Button(String.valueOf("B"));
        button1.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button2 = new Button(String.valueOf("U"));
        button2.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button3 = new Button();
        button3.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button4 = new Button();
        button4.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button5 = new Button(String.valueOf("Z"));
        button5.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button6 = new Button(String.valueOf("Z"));
        button6.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button7 = new Button();
        button7.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button8 = new Button();
        button8.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button9 = new Button();
        button9.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button10 = new Button();
        button10.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button11 = new Button(String.valueOf("W"));
        button11.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button12 = new Button(String.valueOf("O"));
        button12.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button13 = new Button();
        button13.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button14 = new Button();
        button14.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button15 = new Button(String.valueOf("R"));
        button15.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button16 = new Button(String.valueOf("D"));
        button16.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);


        keyboardPanel.add(button1, 0, 0);
        keyboardPanel.add(button2, 1, 0);
        keyboardPanel.add(button3, 2, 0);
        keyboardPanel.add(button4, 3, 0);
        keyboardPanel.add(button5, 0, 1);
        keyboardPanel.add(button6, 1, 1);
        keyboardPanel.add(button7, 2, 1);
        keyboardPanel.add(button8, 3, 1);
        keyboardPanel.add(button9, 0, 2);
        keyboardPanel.add(button10, 1, 2);
        keyboardPanel.add(button11, 2, 2);
        keyboardPanel.add(button12, 3, 2);
        keyboardPanel.add(button13, 0, 3);
        keyboardPanel.add(button14, 1, 3);
        keyboardPanel.add(button15, 2, 3);
        keyboardPanel.add(button16, 3, 3);

        HBox root = new HBox(leftPanel, rightPanel);
        HBox.setHgrow(leftPanel, Priority.ALWAYS);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

}
