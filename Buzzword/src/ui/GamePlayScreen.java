package ui;

/**
 * @author  Shuli Chen
 */

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class GamePlayScreen extends Application {

    final static private int BUTTON_WIDTH = 40;
    final static private int BUTTON_HEIGHT = 40;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("BuzzWord Game");
        primaryStage.setWidth(1200);
        primaryStage.setHeight(600);

        Label professaurLabel = new Label("Professaur");
        professaurLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        professaurLabel.setPrefWidth(190);
        professaurLabel.setPrefHeight(40);

        Label homeLabel = new Label("Home");
        homeLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        homeLabel.setPrefWidth(190);
        homeLabel.setPrefHeight(40);
        homeLabel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                HomeScreen homeScreen = new HomeScreen();
                try {
                    homeScreen.start(new Stage());
                    primaryStage.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        VBox leftMenu = new VBox(professaurLabel, homeLabel);
        leftMenu.setPrefWidth(220);
        leftMenu.setPrefHeight(300);
        leftMenu.setMaxSize(220, 300);
        leftMenu.setSpacing(20);
        leftMenu.setAlignment(Pos.TOP_RIGHT);
        leftMenu.setStyle("-fx-background-color:DarkGray");

        HBox leftPanel = new HBox(leftMenu);
        leftPanel.setPrefWidth(220);
        leftPanel.setAlignment(Pos.CENTER);
        leftPanel.setStyle("-fx-background-color:DarkGray");

        GridPane keyboardPanel = new GridPane();
        keyboardPanel.setPrefWidth(580);
        keyboardPanel.setHgap(40);
        keyboardPanel.setVgap(40);
        keyboardPanel.setAlignment(Pos.CENTER);
        //keyboardPanel.setStyle("-fx-background-color:gray");

        Label title = new Label("!! Buzz Word !!");
        title.setStyle("-fx-text-fill:white");
        title.setAlignment(Pos.CENTER);
        title.setPrefWidth(580);
        title.setPrefHeight(50);

        Label subTitle = new Label("Dictionary Words");
        subTitle.setStyle("-fx-text-fill:white");
        subTitle.setAlignment(Pos.CENTER);
        subTitle.setPrefWidth(580);
        subTitle.setPrefHeight(50);

        FlowPane titlePanel = new FlowPane(title);
        titlePanel.setPrefHeight(100);
        titlePanel.setPrefWidth(580);
        titlePanel.setStyle("-fx-background-color:gray");

        VBox centerPanel = new VBox(titlePanel, subTitle, keyboardPanel);
        centerPanel.setPrefWidth(580);
        centerPanel.setStyle("-fx-background-color:gray");

        Label timeLabel = new Label("TIME REMAINING: 40 seconds");
        timeLabel.setStyle("-fx-background-color:LightSlateGray;-fx-text-fill:red;");
        timeLabel.setAlignment(Pos.CENTER);
        timeLabel.setPrefWidth(300);
        timeLabel.setPrefHeight(60);

        Label currentGuessLabel = new Label("B  U");
        currentGuessLabel.setStyle("-fx-background-color:DarkSlateGray;-fx-text-fill:white;");
        currentGuessLabel.setAlignment(Pos.CENTER);
        currentGuessLabel.setPrefWidth(300);
        currentGuessLabel.setPrefHeight(60);

        Label totalLabel = new Label("WAR   10\r\nRAW   10\r\nDRAW   20");
        totalLabel.setStyle("-fx-background-color:LightSlateGray;-fx-text-fill:white;");
        totalLabel.setAlignment(Pos.CENTER);
        totalLabel.setPrefWidth(300);
        totalLabel.setPrefHeight(200);

        Label targetLabel = new Label("Target\r\n75 points");
        targetLabel.setStyle("-fx-background-color:LightSlateGray;-fx-text-fill:white;");
        targetLabel.setAlignment(Pos.CENTER);
        targetLabel.setPrefWidth(300);
        targetLabel.setPrefHeight(60);

        VBox rightPanel = new VBox(timeLabel, currentGuessLabel, totalLabel, targetLabel);
        rightPanel.setPrefWidth(400);
        rightPanel.setSpacing(20);
        rightPanel.setAlignment(Pos.CENTER_RIGHT);
        rightPanel.setStyle("-fx-background-color:DarkGray");


        Button button1 = new Button(String.valueOf("B"));
        button1.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button2 = new Button(String.valueOf("U"));
        button2.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button3 = new Button();
        button3.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button4 = new Button();
        button4.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button5 = new Button(String.valueOf("Z"));
        button5.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button6 = new Button(String.valueOf("Z"));
        button6.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button7 = new Button();
        button7.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button8 = new Button();
        button8.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button9 = new Button();
        button9.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button10 = new Button();
        button10.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button11 = new Button(String.valueOf("W"));
        button11.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button12 = new Button(String.valueOf("O"));
        button12.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button13 = new Button();
        button13.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button14 = new Button();
        button14.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button15 = new Button(String.valueOf("R"));
        button15.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button16 = new Button(String.valueOf("D"));
        button16.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);


        keyboardPanel.add(button1, 0, 0);
        keyboardPanel.add(button2, 1, 0);
        keyboardPanel.add(button3, 2, 0);
        keyboardPanel.add(button4, 3, 0);
        keyboardPanel.add(button5, 0, 1);
        keyboardPanel.add(button6, 1, 1);
        keyboardPanel.add(button7, 2, 1);
        keyboardPanel.add(button8, 3, 1);
        keyboardPanel.add(button9, 0, 2);
        keyboardPanel.add(button10, 1, 2);
        keyboardPanel.add(button11, 2, 2);
        keyboardPanel.add(button12, 3, 2);
        keyboardPanel.add(button13, 0, 3);
        keyboardPanel.add(button14, 1, 3);
        keyboardPanel.add(button15, 2, 3);
        keyboardPanel.add(button16, 3, 3);

        HBox root = new HBox(leftPanel, centerPanel, rightPanel);
        HBox.setHgrow(leftPanel, Priority.ALWAYS);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
