package ui;

/**
 * @author  Shuli Chen
 */

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

public class LevelSelectionScreen extends Application {

    final static private int BUTTON_WIDTH = 40;
    final static private int BUTTON_HEIGHT = 40;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("BuzzWord Game");
        primaryStage.setWidth(800);
        primaryStage.setHeight(600);

        Label professaurLabel = new Label("Professaur");
        professaurLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        professaurLabel.setPrefWidth(190);
        professaurLabel.setPrefHeight(40);

        Label homeLabel = new Label("Home");
        homeLabel.setStyle("-fx-background-color:DimGray;-fx-text-fill:white;");
        homeLabel.setPrefWidth(190);
        homeLabel.setPrefHeight(40);
        homeLabel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                HomeScreen homeScreen = new HomeScreen();
                try {
                    homeScreen.start(new Stage());
                    primaryStage.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        VBox leftMenu = new VBox(professaurLabel, homeLabel);
        leftMenu.setPrefWidth(220);
        leftMenu.setPrefHeight(300);
        leftMenu.setMaxSize(220, 300);
        leftMenu.setSpacing(20);
        leftMenu.setAlignment(Pos.TOP_RIGHT);
        leftMenu.setStyle("-fx-background-color:DarkGray");

        HBox leftPanel = new HBox(leftMenu);
        leftPanel.setPrefWidth(220);
        leftPanel.setAlignment(Pos.CENTER);
        leftPanel.setStyle("-fx-background-color:DarkGray");

        GridPane keyboardPanel = new GridPane();
        keyboardPanel.setPrefWidth(580);
        keyboardPanel.setHgap(40);
        keyboardPanel.setVgap(40);
        keyboardPanel.setAlignment(Pos.CENTER);
        //keyboardPanel.setStyle("-fx-background-color:gray");

        Label title = new Label("!! Buzz Word !!");
        title.setStyle("-fx-text-fill:white");
        title.setAlignment(Pos.CENTER);
        title.setPrefWidth(580);
        title.setPrefHeight(50);

        Label subTitle = new Label("Famous People");
        subTitle.setStyle("-fx-text-fill:white");
        subTitle.setAlignment(Pos.CENTER);
        subTitle.setPrefWidth(580);
        subTitle.setPrefHeight(50);

        FlowPane titlePanel = new FlowPane(title);
        titlePanel.setPrefHeight(100);
        titlePanel.setPrefWidth(580);
        titlePanel.setStyle("-fx-background-color:gray");

        VBox rightPanel = new VBox(titlePanel, subTitle, keyboardPanel);
        rightPanel.setPrefWidth(580);
        rightPanel.setStyle("-fx-background-color:gray");


        Button button1 = new Button(String.valueOf("1"));
        button1.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button2 = new Button(String.valueOf("2"));
        button2.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button3 = new Button(String.valueOf("3"));
        button3.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button4 = new Button(String.valueOf("4"));
        button4.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button5 = new Button(String.valueOf("5"));
        button5.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button6 = new Button(String.valueOf("6"));
        button6.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button7 = new Button(String.valueOf("7"));
        button7.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        Button button8 = new Button(String.valueOf("8"));
        button8.setPrefSize(BUTTON_WIDTH, BUTTON_HEIGHT);

        keyboardPanel.add(button1, 0, 0);
        keyboardPanel.add(button2, 1, 0);
        keyboardPanel.add(button3, 2, 0);
        keyboardPanel.add(button4, 3, 0);
        keyboardPanel.add(button5, 0, 1);
        keyboardPanel.add(button6, 1, 1);
        keyboardPanel.add(button7, 2, 1);
        keyboardPanel.add(button8, 3, 1);

        HBox root = new HBox(leftPanel, rightPanel);
        HBox.setHgrow(leftPanel, Priority.ALWAYS);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
